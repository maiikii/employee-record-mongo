class JobAssignmentsController < ApplicationController
	before_action :set_job, only: [:edit, :update, :destroy]

  def index
    @job_assignments = JobAssignment.all
  end

  def new
    @job_assignment = JobAssignment.new
  end

  def create
    @job_assignment = JobAssignment.new(job_assignment_params)

    if @job_assignment.save
      redirect_to job_assignments_path, notice: "The assigned job title has been created!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    if @job_assignment.update_attributes(job_assignment_params)
      redirect_to job_assignments_path, notice: 'The assigned job title has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @job_assignment.destroy
    redirect_to job_assignments_path, notice: 'The assigned job title has been deleted!' and return
  end

  private
    def set_job
      @job_assignment = JobAssignment.find(params[:id])
    end

    def job_assignment_params
      params.require(:job_assignment).permit(:start_date, :end_date, :employee_id, :job_title_id)
    end
end
