module UsersHelper
	def display_name(employee)
		"#{employee.first_name} #{employee.middle_name} #{employee.last_name}"
	end

	def manage_user(user)
		first = User.first
		return !(first.id == user)
	end
end
