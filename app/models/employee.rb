class Employee
	include Mongoid::Document
	field :first_name, 		type: String
	field :middle_name, 	type: String
	field :last_name, 		type: String
	field :birthday, 		type: Date

	belongs_to :department
	has_many :job_assignment, autosave: true
  	embeds_many :addresses
  	accepts_nested_attributes_for :addresses

	validates :first_name, length: { minimum: 2, :message => "What" }, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
	validates :last_name, length: { minimum: 5, :message => "What" }, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
end